using UnrealBuildTool;

public class RacerTarget : TargetRules
{
	public RacerTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("Racer");
	}
}
